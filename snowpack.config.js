/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  mount: {
    public: '/',
    src: '/dist',
    'node_modules/primeicons/fonts/': '/fonts/'
  },
  routes: [
    {
      // 'All' refers to all URLs (including JS, CSS, Image files and more)
      // will respond with the fallback HTML file
      // match: 'all',

      // 'Routes' refers to all URLs that either do not include a file extension
      // or that include the “.html” file extension.
      match: 'routes',
      src: '.*',
      dest: '/',
    },
  ],
  exclude: ['**/node_modules/**/*'],
  plugins: [
    '@snowpack/plugin-sass',
  ],
  packageOptions: {},
  devOptions: {},
  buildOptions: {},
};
