import React from 'react'
import ReactDom from 'react-dom'
import { Rating } from 'primereact/rating'
import './index.scss'

function App () {
  return (
    <>
      <Rating value={5} readOnly stars={10} cancel={false} />
    </>
  )
}

ReactDom.render(
  <App />,
  document.getElementById('root')
)
